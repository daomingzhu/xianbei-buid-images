#!/usr/bin/env bash
image_name="$1"
VERSION="$2"
echo "请输入用户名(admin@xianbei.cc):"
read -e USERNAME
docker login --username=$USERNAME registry.cn-hangzhou.aliyuncs.com

if [[ -z "$VERSION" ]]||[[ -z "$image_name" ]]; then
echo "请传入必要参数！！"
echo "----参数1：要提交的镜像名称！"
echo "----参数2：要提交的镜像版本！例:v1.0,v1.1 ...:"
exit 1
else
IMAGEID=$(docker images |grep "$image_name" |grep "$VERSION"|awk '{print $3}'|head -n 1)

if [[ -z "$IMAGEID" ]]; then
echo "未找到您要提交的镜像"
exit 1
else
echo "找到镜像！镜像id:$IMAGEID"
echo "请输入阿里容器云的namespace(xianbei_eureka):"
read -e NAMESPACE
echo "请输入阿里容器云的镜像名称:"
read -e REPONAME
sudo docker tag $IMAGEID registry.cn-hangzhou.aliyuncs.com/$NAMESPACE/$REPONAME:$VERSION
sudo docker push registry.cn-hangzhou.aliyuncs.com/$NAMESPACE/$REPONAME:$VERSION
fi

exit 0

fi

