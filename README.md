# Dockerfile #

Docker 为创建容器镜像的配置文件
### 包含文件 ###

* 生成容器镜像文件的Dockerfile
* 运行Dockerfile，将镜像提交到私服镜像仓库
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### 怎么使用? ###

* 运行启动脚本 start.sh
* 修改镜像内容需要在Dockerfile中修改配置

