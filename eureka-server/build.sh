#!/usr/bin/env bash
image_name="eureka/server"
image_v="v1"
git pull
cd /home/xianbei/xianbei_eureka_server
mvn clean package -Dmaven.test.skip=true
targetjar=$(find ./ -name "*.jar" |head -n 1)
if [[ -f "$targetjar" ]] ; then
echo "打包成功"
cd /home/xianbei/docker-ansible/eureka-server/dist
find /home -regex ".*/xianbei_eureka_server/.*.jar" |xargs -i mv {} ./
echo "复制打包后的jar到当前目录："
ls -lrh
docker build -t "$image_name:$image_v" .
../../push-aliyun.sh "$image_name" "$image_v"
exit 0
else
echo "未打包成功"
exit 1
fi

