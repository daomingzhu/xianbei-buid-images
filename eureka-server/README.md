# eureka server #

使用Docker创建eureka容器
### 实施步骤 ###

*  下载基于centos的jdk8的环境(registry.cn-hangzhou.aliyuncs.com/xianbei_jdk/centos_jdk8:v1.2)
*  从git down eureka源码 使用mvn将其打包
*  编写dockerfile配置 将打包的eureka erver COPY 到镜像 然后发布到阿里容器云环境
*  编写docker-compose配置 容器编排部署到swarm集群
*  具体实现详情请看build.sh源码

### 怎么使用? ###

* 运行启动脚本 build.sh
* 修改镜像内容需要在Dockerfile中修改配置

